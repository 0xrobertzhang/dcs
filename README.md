# DCS

A strong functional programming language supporting divide and conquer recursion.

## Installation

Run `cabal v1-build` in the `backend` subdirectory.

Then add the following to your `~/.emacs` file, with `TOPLEVEL-DIR-OF-DCS` replaced by the actual location of the top-level directory of the dcs repo on your computer:

```
(load "TOPLEVEL-DIR-OF-DCS/dcs-mode/dcs-mode-setup.el")
```

You may also need to do `M-x customize-group RET dcs RET` in Emacs,
and set the variable `dcs-mode-backend` to the compiled executable,
which should be
`TOPLEVEL-DIR-OF-DCS/backend/dist/build/dcs/dcs`.

## Registering the standard library

A standard library is just getting started. To import its files, you also need to add

```
(dcs-mode-register-root "TOPLEVEL-DIR-OF-DCS/Stdlib")
```

to your .emacs file. Then you can import files as shown in [examples like this](tests/importStdlib.dcs).

## Documentation

The following documentation is currently available:

- [docs/EmacsMode.md](docs/EmacsMode.md) for using the emacs mode

- [docs/Datatypes.md](docs/Datatypes.md) about DCS's datatype system

## Discussion

Please join the [DCS Coders](https://t.me/+0oidecmDY6QyYWFh) Telegram group if you are trying out DCS and want to discuss.

## Contributing

Please read our guide to [contributing](CONTRIBUTING.md) if you are interested in working on DCS.