# Guide for Contributing to DCS

Thanks a lot for your interest in DCS. We are very pleased if you can
contribute to the project.  There are several ways to get involved.

## Contribute code written in the DCS language

Writing code in DCS is very helpful, for testing out the tool (and if
you find a bug, then of course please just file an issue),
demonstrating the capabilities of the language, and developing the
standard library.

You could add to:

 * `Stdlib/` for standard library functions.  As Haskellers, we are generally interested in functions similar to what one finds in the GHC Prelude, for example.

 * `examples/` for simple things, that demonstrate a feature of the language

 * `studies/` for more challenging examples.  We are particularly interested in solutions to problems from Richard Bird's "Pearls of Functional Algorithm Design", or similarly difficult challenge problems

 * `tests/` for basic tests of DCS; for example, to show that error messages are generated as expected in various cases

## Contribute to the DCS implementation

For this, you will probably want to browse the issues with label `Feature`.

## Preferred way to propose contributions

We prefer that you create MRs on the gitlab repo for the project, to share your contribution.  We are not planning to add lots of committers to the project, so please use an MR instead of requesting permission to commit directly.

To create an MR when you do not have access to the project, create a fork of the project and make changes there.  When you are ready, please create an MR from the branch of your fork to `main` of the upstream project.

Contributors will be acknowledged by adding them to the the Contributors.md file.

## Style

We prefer merge requests (MRs) that are *focused* and *semantically relevant*.  Focused means that the changes are, as much as possible, localized.  So there should be a very good reason for MRs that touch a lot of the code.  The preference is for changes in a bounded part of the codebase.  So for example, MRs for code cleanup across the whole codebase will be less preferred.  By semantically relevant, we mean changes that affect the functionality of the code, not just appearance.  So for example, MRs that affect only whitespace and comments will generally be less preferred.

