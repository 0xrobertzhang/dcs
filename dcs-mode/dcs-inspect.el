(defun dcs-mode-inspect-buffer-name ()
  "Return the name for the inspect buffer for the current dcs editing buffer"
  (concat "*dcs-inspect-" (dcs-mode-current-buffer-base-name) "*"))

(defun dcs-mode-inspect-buffer ()
  "Return the inspect buffer for the current dcs editing buffer"
  (get-buffer-create (dcs-mode-inspect-buffer-name)))

(defun dcs-mode-toggle-inspect-buffer ()
  "Toggle visibility of the inspect buffer for the current dcs editing buffer"
  (interactive)
  (let* ((b (dcs-mode-inspect-buffer))
         (w (get-buffer-window b)))
    (if w
        (delete-window w)
        (display-buffer b)))
  )

(defun dcs-mode-inspect (str)
  "Show the inspect buffer, with the given string; delete any old contents first"
  (let* ((b (dcs-mode-inspect-buffer))
         (w (get-buffer-window b))
         (s (selected-window)))
    (unless w
      (display-buffer b))

    (select-window (get-buffer-window b))

    (setq buffer-read-only nil)
    (erase-buffer)
    (insert str)
    (fit-window-to-buffer)
    (goto-char 1)
    (setq buffer-read-only t)
    (select-window s)
  ))


(provide 'dcs-inspect)