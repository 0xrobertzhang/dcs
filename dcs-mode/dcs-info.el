;; -*- lexical-binding: t; -*-

(defun dcs-mode-show-type-info()
  "Show typing information for the extent currently in navigation focus if point is within that extent, or else request info for the symbol at point."
  (interactive)
  (if (dcs-mode-point-in-navigation-focusp)
      (let* ((e (dcs-mode-extent-of-navigation-focus))
             (sp (car e))
             (ep (cdr e)))
        (dcs-mode-send (concat "type-info " (number-to-string sp) " " (number-to-string ep))))
    (progn
      (dcs-mode-clear-navigation-focus "(dcs-info-at-point)")))) ; to avoid confusion about what information we are showing

(defun dcs-info-at-point()
  "Callback issued by backend when rehighlighting is finished, to request type info at point"
  (dcs-mode-send (concat "info " (number-to-string (point)))))

(provide 'dcs-info)
