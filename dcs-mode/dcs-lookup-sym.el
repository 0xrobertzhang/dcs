;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;  -*- lexical-binding: t; -*-
;; jumping to where symbols are introduced
;;
;; 
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defvar-local dcs-mode-starting-pos-for-lookup nil
  "The position from which we initiate a symbol lookup")

(defvar-local dcs-mode-ending-pos-for-lookup nil
  "The position we reached for a symbol lookup.")

(defun dcs-mode-cur-pos()
  "Returns a pair of current buffer name and position in that buffer"
  (cons (buffer-file-name) (point)))

;; This function assumes we have already loaded the file
(defun dcs-mode-lookup-sym (pos)
  "Invoke the backend to jump to the location where the symbol at the given position is introduced.  If there is no symbol at that position, an error is signaled"
  (let ((spos (number-to-string pos)))
    ;; (dcs-mode-debug-message "jump to symbol at %d" pos)
    (setq dcs-mode-starting-pos-for-lookup (dcs-mode-cur-pos))
    (dcs-mode-send (concat "jump " spos))))

(defun dcs-mode-lookup-sym-at-point ()
  "Jump to the location where the symbol at point is introduced.  If there is no symbol at that position, signal an error"
  (interactive)
  (if (equal dcs-mode-ending-pos-for-lookup (dcs-mode-cur-pos))
        ; We are at the symbol we just jumped to in the previous jump request.
        ; In this case, we jump back.
        (progn
          (setq dcs-mode-ending-pos-for-lookup nil)
          (dcs-mode-jump-to-pos-in-file dcs-mode-starting-pos-for-lookup)
          (setq dcs-mode-ending-pos-for-lookup nil)
          (setq dcs-mode-starting-pos-for-lookup nil))          
        ; otherwise, initiate a new jump
        (dcs-mode-lookup-sym (point))))

(defun dcs-mode-jump-to-pos-in-file (filepos)
  "Jump to the given location in a possibly different dcs file."
  (let ((file (car filepos))
        (pos (cdr filepos))
        (startingpos dcs-mode-starting-pos-for-lookup))
    (unless (equal (buffer-file-name) file)
      (dcs-mode-debug-message "reloading upon jump")
      (find-file file)
      (dcs-mode-struct-edit)
      ; copy this value to the new buffer, so commands executed there can jump back
      (setq dcs-mode-starting-pos-for-lookup startingpos))
    (setq dcs-mode-ending-pos-for-lookup (cons file pos))
    (goto-char pos)
    ))
  
(provide 'dcs-lookup-sym)
