# Contributors

The following people have contributed to DCS, either through work on the DCS tool or adding DCS code to this repo.  The list grows by addition at the end, and contributions may be summarized by MR id. 

- Aaron Stump, main implementor, project lead

- Stefan Monnier, emacs improvements

- Robert Zhang, MR 2