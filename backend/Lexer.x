{

module Lexer where

import Pos
import Control.Monad
import Data.Text(Text,pack)

}

%wrapper "monadUserState"

$alpha		= [a-zA-Z]
$pathchar       = [0-9\-\_$alpha]
$varchar	= [$pathchar\']  

@pathunit       = $pathchar+
@word           = $varchar+
@var            = @word | \_ | @word\.@word

@import         = ι(\ )*(\.\/ | \.\.\/ | \/)?(@pathunit \/)* @pathunit

token :-
      λ                                     { mkTokenEmpty Tlambda       }
      γ                                     { mkTokenEmpty Tgamma        }
      ω                                     { mkTokenEmpty Tomega        }
      \~                                    { mkTokenEmpty Ttilde }
      \|                                    { mkTokenEmpty Tpipe         }            
      \{                                    { mkTokenEmpty Tlbrace        }
      \}                                    { mkTokenEmpty Trbrace        }
      \(                                    { mkTokenEmpty Tlparen        }
      \)                                    { mkTokenEmpty Trparen        }
      \:                                    { mkTokenEmpty Tcolon        }
      \=                                    { mkTokenEmpty Teq        }
      \.                                    { mkTokenEmpty Tdot        }
      →                                     { mkTokenEmpty Tarrow        }      
      ⇒                                     { mkTokenEmpty Tfatarrow        }      
      δ                                     { mkTokenEmpty Tdata }
      τ                                     { mkTokenEmpty Ttype }      
      σ                                     { mkTokenEmpty Tsigma }      
      \!                                    { mkTokenEmpty Tbang }
      \n$white*\n                           { mkTokenEmpty Tnlnl }
      $white+                               { skip'                      }
      @var                                  { mkToken Tvar               }
      @import                               { mkToken Timport            }

{

data TokenClass =
        Tvar    String
     |  Timport String
     |  Ttilde
     |  Tlbrace
     |  Trbrace
     |  Tlparen
     |  Trparen
     |  Tcolon
     |  Teq
     |  Tstar
     |  Tdot     
     |  Tarrow
     |  Tfatarrow
     |  Tdata
     |  Ttype
     |  Tsigma
     |  Tbang
     |  Tnlnl
     |  Tlambda
     |  Tgamma
     |  Tomega
     |  Tpipe     
     |  TEOF
     deriving (Eq, Show)


mkToken :: (String -> TokenClass) -> AlexAction Token
mkToken c (p, _, _, input) len =
  do
    f <- alexGetFilename
    return $ Token f p (c (take len input))

mkTokenEmpty :: TokenClass -> AlexAction Token
mkTokenEmpty c = mkToken (\ _ -> c)

data Token = Token String {- filename -} AlexPosn TokenClass
  deriving (Show, Eq)

tpos :: Token -> Pos
tpos (Token f (AlexPn p _ _) _) = mkPos f (p + 1)

tFilename :: Token -> String
tFilename (Token f _ _) = f

-- specify the length of the token that matched the position
pos2TxtLen :: AlexPosn -> Int -> Text
pos2TxtLen (AlexPn p _ _) ln = pack (show (p + ln + 1))

posEnd :: Token -> Text
posEnd t@(Token _ (AlexPn p a b) c) = pack (show (1 + p + length (tcStr c)))

tTxt :: Token -> Text
tTxt (Token _ _ t) = pack (tcStr t)

tStr :: Token -> String
tStr (Token _ _ t) = tcStr t

tcStr :: TokenClass -> String
tcStr (Tvar    s)     = s
tcStr (Timport s)     = s
tcStr _               = ""

data AlexUserState = AlexUserState { filename :: String }

alexInitUserState = AlexUserState { filename = "none" }

alexSetFilename :: String -> Alex ()
alexSetFilename n =
  do
    st <- alexGetUserState
    alexSetUserState st { filename = n }

alexGetFilename :: Alex String
alexGetFilename =
  do
    st <- alexGetUserState
    return $ filename st

alexEOF :: Alex Token
alexEOF = do
  (p, _, _, _) <- alexGetInput
  f <- alexGetFilename
  return $ Token f p TEOF

isEOF :: Token -> Bool
isEOF (Token _ _ TEOF) = True
isEOF _                = False

skip' _input _len = alexMonadScanErrOffset

alexMonadScanErrOffset = do
  inp <- alexGetInput
  sc  <- alexGetStartCode
  case alexScan inp sc of
    AlexEOF -> alexEOF
    AlexError ((AlexPn off _ _),_,_,_) -> alexError $ "L" ++ show (off + 1)
    AlexSkip  inp' len -> do
        alexSetInput inp'
        alexMonadScan
    AlexToken inp' len action -> do
        alexSetInput inp'
        action (ignorePendingBytes inp) len


}
