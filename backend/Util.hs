module Util where

import Control.Monad.Reader
import Control.Monad.State.Lazy
import Data.Tree

{- Call p on suffixes of xs until it returns Just (ys,zs).
   Return the tuple of the prefix of the original input
   before p returns Just, ys, and zs.

   If xs is empty just return ([],[],[]).


Behavior off for an example like:

λ> breakL (\ r -> case r of { '\n' : _ -> Just([],r) ; _ -> Nothing }) "   "
("   ","","")

We probably want this to indicate instead that p never returned Just?

-}
breakL :: ([a] -> Maybe ([a],[a])) -> [a] -> Maybe ([a],[a],[a])
breakL p [] = Nothing
breakL p xs@(x:xs') =
  case p xs of
    Just (ys,zs) -> Just ([],ys,zs)
    Nothing ->
      do
        (pre,ys,zs) <- breakL p xs' 
        return (x:pre,ys,zs)

-- convert from Reader to State (found on stackoverflow)
rd :: Reader s a -> State s a
rd a = gets (runReader a)

-- useful combinator for combining a list of actions that produce lists
aseq :: Applicative m => [ m [b] ] -> m [b]
aseq l = join <$> sequenceA l

{- Lift a computation from a to [a] -}
concatMapM :: Monad m => (a -> m [b]) -> ([a] -> m [b])
concatMapM m f = join <$> mapM m f 

maybeMapM :: Monad m => (a -> m [b]) -> (Maybe a -> m [b])
maybeMapM m Nothing = return []
maybeMapM m (Just x) = m x

-- compute a fixed point through a monad
fixM :: (Eq a , Monad m) => (a -> m a) -> a -> m a
fixM f a =
  do
    a' <- f a
    if a == a' then
      return a'
    else
      fixM f a'

-- assumes input greater than or equal to 0
digitsr :: Int -> [Int]
digitsr 0 = []
digitsr x =
  let r = x `mod` 10
      d = x `div` 10
  in
    r : digitsr d

digits = reverse . digitsr

toUnicodeSubscript :: Int -> String
toUnicodeSubscript x = join (h <$> digits x)
  where h 0 = "₀"
        h 1 = "₁"
        h 2 = "₂"
        h 3 = "₃"
        h 4 = "₄"
        h 5 = "₅"
        h 6 = "₆"
        h 7 = "₇"
        h 8 = "₈"
        h 9 = "₉"
        h _ = "ₕ" -- should not happen

-- return elements of the tree, in left-to-right postfix order
postfix :: Tree a -> [a]
postfix (Node x ts) = join (postfix <$> ts) ++ [x]