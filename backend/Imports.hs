module Imports (RootList, ImportPaths, showImportError, showRootError,
                addRoot, gatherFiles, processFileWithDeps) where

import Pos
import Syntax
import SyntaxHelpers
import Comments
import Parser
import System.FilePath
import System.Directory
import Control.Applicative
import Control.Monad.State.Lazy
import qualified Data.Map as M
import Data.Text(Text,unpack,pack)
import Data.Maybe
import Data.List
import Data.Tree

type RootList = [(FilePath,String)] -- full path, library name (last unit in path) with trailing separator

type ImportPoint =
  (RootList, Extent {- position in file -}, String {- import string -}, FilePath {- file we are importing from -})

data ImportError =
    MissingLocalImport ImportPoint FilePath {- the path we checked -}
  | MissingGlobalImport ImportPoint
  | ImportErrorsInImported ImportPoint
  | CyclicImports [FilePath] Int -- The Int is for which file in the list starts the cycle.
                                 -- Sadly, it seems a pain to get the ImportPoint in the file being viewed in emacs,
                                 -- for the import which starts the list of paths leading to the cycle.

data RootError =
    RootNotDirectory FilePath
  | RootNotReadable FilePath
  | RootTrivial FilePath
  | RootEmpty FilePath      
  | RootDuplicate FilePath FilePath      

instance Show ImportError where
  show = showImportError

instance Show RootError where
  show = showRootError

startImportError :: Extent -> String
startImportError (sp,ep) = "(dcs-mode-import-error " ++ elispPos sp ++ " " ++ elispPos ep ++ " \""

endImportError :: String
endImportError = "\\n\")"

showImportInfo :: String -> FilePath -> String
showImportInfo imprt whre = " The unit " ++ imprt ++ " imported from " ++ whre

showRoots :: RootList -> String
showRoots [] = "\\n - no registered roots (see README.md)"
showRoots roots = "\\n - The registered roots are:" ++ foldr (\ (r,_) str -> "\\n   " ++ r ++ str) "" roots

aroundImportError :: ImportPoint -> String -> String
aroundImportError (_, ext, imprt, whre) e =
  startImportError ext ++
  showImportInfo imprt whre ++
  e ++
  endImportError

showImportError :: ImportError -> String
showImportError (MissingLocalImport ip@(roots,_,_,whre) path) =
  aroundImportError ip $
  " cannot be found\\n  The path we checked is: " ++ path
showImportError (MissingGlobalImport ip@(roots,_,_,whre)) =
  aroundImportError ip $
  " cannot be found under any root" ++ showRoots roots
showImportError (ImportErrorsInImported ip) =
  aroundImportError ip
  " has import or parse errors"
showImportError (CyclicImports paths@(p:ps) i) = 
  startImportError (mkPos p 1, mkPos p 1) ++
  " There is a cycle in the import graph: " ++
  foldr (\ (p,n) str -> "\\n   " ++ (if n == i then "* " else "  ") ++ p ++ str) "" (zip ps [1..]) ++
  endImportError

showImportError (CyclicImports [] i) = undefined -- cannot happen

startRootError :: String
startRootError = "(dcs-mode-root-error \""

endRootError :: String
endRootError = "\\n\\n\")"

aroundRootError :: String -> String
aroundRootError errmsg = 
  startRootError ++ errmsg ++ endRootError
  
showRootError (RootNotDirectory r) =
  aroundRootError 
  ("This registered root is not a directory:\\n  " ++ r)

showRootError (RootNotReadable r) =
  aroundRootError 
  ("This registered root is not readable:\\n  " ++ r)

showRootError (RootTrivial r) =
  startRootError ++ 
  "This registered root is trival (must have more than just a slash):\\n  " ++ r ++ 
  endRootError

showRootError (RootEmpty r) =
  startRootError ++ 
  "This registered root is empty:\\n  " ++ r ++ 
  endRootError

showRootError (RootDuplicate r1 r2) =
  startRootError ++ 
  "Registering a root with a duplicate root name:\\n  " ++
  "1. The new registered root: " ++ r1 ++ "\\n  " ++
  "2. The previously registered root with same root name (last part of path): " ++ r2 ++     
  endRootError

----------------------------------------------------------------------
-- Compute information (in ImportState) about imports
--
-- Functions importGraph and importGraphPath (below) are used to
-- traverse the recursively imported files for a file.
----------------------------------------------------------------------

-- map canonical filepaths to the SimpleFiles they contain (if we could parse the file)
type SimpleFilesMap = M.Map FilePath SimpleFile

-- map canonical filepaths to comments found in them
type CommentsMap = M.Map FilePath [Comment]

-- map canonical FilePaths to their Imports
type ImportMap = M.Map FilePath Imports

-- map starting positions for imports (the iota directives in files) to the paths for the imported files
type ImportPaths = M.Map Pos FilePath

data ImportState = ImportState { simpleFilesMap :: SimpleFilesMap,
                                 importMap :: ImportMap,
                                 importPaths :: ImportPaths,
                                 commentsMap :: CommentsMap
                                 }
  deriving Show

initImportState :: ImportState
initImportState =
  ImportState { simpleFilesMap = M.empty , importMap = M.empty, importPaths = M.empty, commentsMap = M.empty }

type ImportStateful a = StateT ImportState IO a

io :: IO a -> ImportStateful a
io = liftIO

addComments :: FilePath -> [Comment] -> ImportStateful ()
addComments whre cs =
  do
    st <- get
    put $ st { commentsMap = M.insert whre cs $ commentsMap st } 

addImports :: FilePath -> Imports -> ImportStateful ()
addImports whre imps =
  do
    st <- get
    put $ st { importMap = M.insert whre imps $ importMap st } 

addImportPath :: Pos -> FilePath -> ImportStateful ()
addImportPath pos path =
  do
    st <- get
    put $ st { importPaths = M.insert pos path $ importPaths st } 

addSimpleFile :: FilePath -> SimpleFile -> ImportStateful ()
addSimpleFile whre cs =
  do
    st <- get
    put $ st { simpleFilesMap = M.insert whre cs $ simpleFilesMap st } 

findRoot :: String -> RootList -> Maybe FilePath
findRoot rootname roots = fst <$> find (\ (_,n) -> n == rootname) roots

-- assumes imprt is nonempty, returns a canonical path or error.
resolveImport :: ImportPoint -> IO (Either ImportError String)
resolveImport ip@(roots, ext, imprt, whre) =
  let (p1:ps) = splitPath imprt
      whredir = takeDirectory whre
      h local strt ps =
        do 
          x <- canonicalizePath (strt </> concat ps <.> "dcs")
          b <- doesFileExist x
          if b then 
            return $ Right x
          else
            return $ Left (if local then MissingLocalImport ip x else MissingGlobalImport ip)
  in          
    case p1 of
      "/" ->
        -- path from a root
        let (p2:ps') = ps in
          case findRoot p2 roots of
            Nothing -> return (Left (MissingGlobalImport ip))
            Just p -> h False p ps' 
      "../" -> h True whredir (p1:ps) 
      "./" ->  h True whredir ps
      _ ->     h True whredir (p1:ps) -- path from current directory (w/o initial ./)

type ParseOrImportErrors = Either ParseErrorMessage [ImportError]

-- construct the import graph for a given path
importGraphPath :: RootList -> FilePath -> ImportStateful ParseOrImportErrors
importGraphPath roots path = 
  do
    contents <- io $ readFile path

    let (comments,cleaned) = cullComments contents

    addComments path comments

    -- parse file (without comments)
    case parseTxt path (pack cleaned) of 
      Left m ->
        return $ Left m
      Right file ->
        do
          let (imprts,sfile) = file
          addImports path imprts
          addSimpleFile path sfile
          rs <- mapM (\ i@(Import _ imprt _ _) ->
                        importGraph (roots,extentImport i, imprt, path))
                imprts
          return $ Right $ join rs

-- when we have an ImportPoint (so we are doing an import, not processing a file
-- from the frontend at the top level), construct the import graph
importGraph :: ImportPoint -> ImportStateful [ImportError]
importGraph ip@(roots,ext,_,_) =
  do
    mpath <- io $ resolveImport ip
    case mpath of
      Left err -> return [err]   -- problem resolving import to a path
      Right path ->
        do
          addImportPath (startingPosExtent ext) path
          st <- get
          if M.member path (importMap st) then
            -- we already processed this one
            return []
          else
            do
              r <- importGraphPath roots path
              case r of
                Right [] -> return []
                _ -> return [ImportErrorsInImported ip]

checkRootReadable :: FilePath -> IO [RootError]
checkRootReadable r = 
  do
    b <- doesDirectoryExist r
    if b then
      do
        p <- getPermissions r
        if readable p then
          return []
        else
          return [RootNotReadable r]
    else
      return [RootNotDirectory r]

addRoot :: RootList -> FilePath -> IO ([RootError],RootList)
addRoot rs p =
  do
    p' <- canonicalizePath p
    es <- checkRootReadable p'
    let ps = splitPath p' 
    return $ case ps of
               [] -> (RootEmpty p' : es,rs)
               [u] -> (RootTrivial p' : es,rs)
               _ ->
                 let rootname = addTrailingPathSeparator (last ps) in
                   case findRoot rootname rs of
                     Nothing -> (es, (p',rootname) : rs)
                     Just p2 -> (RootDuplicate p' p2 : es, rs)
      
----------------------------------------------------------------------
-- Compute a tree of Imports from the import graph
--
-- Calling Util.postfix on this graph would give you the dependencies
-- in order from files that are needed to files that need them

-- used when traversing ImportGraphs
type Marks = M.Map FilePath ()

data DependencyState =
  DependencyState { marks :: Marks,
                    depPath :: [FilePath], -- current path through dependency graph
                    cycleFound :: Maybe Int -- if depPath contains a cycle, index of repeated elt
                  }
  deriving Show

initDependencyState :: DependencyState
initDependencyState = DependencyState M.empty [] Nothing

{- compute a tree of readable imports for the given file.
   traversing the tree in postfix order left to right
   would give a valid linearization of the imports: all dependencies
   of an imported file would be encountered in such a traversal before
   that file itself. -}
dependencies :: ImportState -> FilePath -> ([DependencyTree], DependencyState)
dependencies impst path =
  runState
    (dependenciesFromPath (importMap impst) (importPaths impst) (simpleFilesMap impst) path)
    initDependencyState

-- return the dependency for an import, if the import is a legal one
importToDependency :: ImportPaths -> SimpleFilesMap -> Import -> Maybe Dependency
importToDependency imppaths sfm imp@(Import pis path pos mq) =
  do
    path' <- M.lookup (startingPosImport imp) imppaths 
    return (Import pis path' pos mq , getSimpleFileSure sfm path')

{- change the path in the Import to its full path, the one we get by resolving the original import path.
   This function assumes that the given import can have its path resolved. -}
importToDependencySure :: ImportPaths -> SimpleFilesMap -> Import -> Dependency
importToDependencySure imppaths sfm imp = 
  fromJust $ importToDependency imppaths sfm imp

-- return list of all imports with legal paths, where the paths are now resolved (to their full paths)
readableDependencies :: ImportPaths -> SimpleFilesMap -> Imports -> [Dependency]
readableDependencies imppaths sfm imps =
  catMaybes $ importToDependency imppaths sfm <$> imps 

-- given just a file path, compute the dependency trees for each of its Imports
dependenciesFromPath :: ImportMap -> ImportPaths -> SimpleFilesMap -> FilePath ->
                     State DependencyState DependencyTrees 
dependenciesFromPath impmap imppaths sfm path =
  case M.lookup path impmap of
    Nothing -> -- this case can only happen if we could not parse the file
      return []
    Just imps ->
      do
        st <- get
        put $ st { depPath = path : depPath st } -- add to current path in dependency graph, to detect cycle
        ps <- mapM (dependenciesFromImport impmap imppaths sfm)
                   (readableDependencies imppaths sfm imps)

        st' <- get

        -- if we did not find a cycle, then restore depPath, set mark on path
        unless (isJust (cycleFound st'))
          (put $ st' { depPath = depPath st {- the depPath from the original state st -} ,
                       marks = M.insert path () (marks st') })
        return $ catMaybes ps

dependenciesFromImport :: ImportMap -> ImportPaths -> SimpleFilesMap -> Dependency ->
                          State DependencyState (Maybe DependencyTree)
dependenciesFromImport impmap imppaths sfm dep@(Import _ path _ _, sf) =
  do
    st <- get
    if isJust (cycleFound st) || M.member path (marks st) then
      return Nothing -- cycle, or already considered this node
    else
      case findIndex (== path) (depPath st) of
        Just i ->
          (put $ st { cycleFound = Just i }) >>
          return Nothing
        Nothing -> -- not on a cycle
          Just . Node dep <$> dependenciesFromPath impmap imppaths sfm path 

-- assuming that there is a SimpleFile for the given path in the given map, return it
getSimpleFileSure :: SimpleFilesMap -> FilePath -> SimpleFile
getSimpleFileSure sfm path = fromJust $ M.lookup path sfm
                    
{- assuming that the given path indeed names an existing file F,
   try to return that file's comments, data structure (ImportPaths) mapping
   positions in the main file to paths for imported files,
   and then either parsing or import errors, or
   else a file with dependencies.
-}
gatherFiles :: RootList -> FilePath ->
  IO (Comments,
      ImportPaths,
      Either ParseOrImportErrors FileWithDeps)
gatherFiles roots path =
  do 
    (pe,impst) <- runStateT (importGraphPath roots path) initImportState
    let (Just comments) = M.lookup path (commentsMap impst) -- should not fail
        imppaths = importPaths impst
    return (comments, imppaths,
            case pe of
              Right [] ->
                -- no parse or import errors
                let imps          = fromJust $ M.lookup path (importMap impst) 
                    mainfile      = getSimpleFileSure sfm path
                    (deps,linst)  = dependencies impst path
                    sfm           = simpleFilesMap impst

                in
                  case cycleFound linst of
                    Nothing -> Right (deps, mainfile)
                    Just i ->  Left $ Right $ [CyclicImports (depPath linst) i]
              _ -> Left pe)

{- processDependencyTrees is a generic function for processing DependencyTrees using a function

      act :: SimpleFile -> m [e]

   that can process the file of one Dependency, and a function

      errCompress :: Extent -> e

   that compresses errors found beneath a Dependency
   (in a DependencyTree), to a single error

   This function can be used to process DependencyTrees for the various different
   analyses of DCS.
-}
processFileWithDeps :: Monad m => (SimpleFile -> m [e]) -> (Extent -> e) -> FileWithDeps -> m [e]
processFileWithDeps act errCompress (deps,main) =
  do
    es <- processDependencyTrees act errCompress deps
    (es ++) <$> act main

processDependencyTree :: Monad m => (SimpleFile -> m [e]) -> DependencyTree -> m [e]
processDependencyTree act (Node (_,file) ts) =
  do
    es <- mapM (processDependencyTree act) ts
    (join es ++) <$> act file
processDependencyTrees :: Monad m => (SimpleFile -> m [e]) -> (Extent -> e) -> DependencyTrees -> m [e]
processDependencyTrees act errCompress deps =
  do
    es <- mapM (\ deptree ->
                  do
                    let (imp,_) = rootLabel deptree
                    errs <- processDependencyTree act deptree
                    return $
                      case errs of
                        [] -> Nothing
                        _ -> Just $ errCompress (extentImport imp))
          deps
    return (catMaybes es)

