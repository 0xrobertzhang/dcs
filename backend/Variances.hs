module Variances where

import Syntax
import SyntaxHelpers
import Imports
import Pos
import ScopeMap
import Util
import Data.List
import Data.Maybe
import Data.Tree
import qualified Data.Map as M
import Control.Monad.State.Lazy

{- a 4-point lattice for variances; see helper functions below for the structure -}
data Variance =
    Positive -- type variable occurs at least once, only positively
  | Negative -- type variable occurs at least once, only negatively
  | Mixed    -- type variable occurs at least once positively and at least once negatively
  | Unused  -- type variable does not occur at all
  deriving Eq

instance Show Variance where
  show Positive = "+"
  show Negative = "-"
  show Mixed = "±"
  show Unused = "∅"

invertVariance :: Variance -> Variance
invertVariance Positive = Negative
invertVariance Negative = Positive
invertVariance Mixed = Mixed
invertVariance Unused = Unused

glbVariance :: Variance -> Variance -> Variance
glbVariance Positive Positive = Positive
glbVariance Positive Negative = Mixed
glbVariance Negative Positive = Mixed
glbVariance Negative Negative = Negative
glbVariance Unused x = x
glbVariance x Unused = x
glbVariance _ Mixed = Mixed
glbVariance Mixed _ = Mixed

okFunctorVariance :: Variance -> Bool
okFunctorVariance Positive = True
okFunctorVariance Unused = True
okFunctorVariance _ = False

{- What variance would you be at for occurrence o in type t2[t1],
   if the hole in t2 has variance v2, and o has variance v1 in t?

   Probably there is some algebraic term for this operation... -}
substVariance :: Variance -> Variance -> Variance
substVariance Positive x = x
substVariance Negative x = invertVariance x
substVariance Unused x = Unused
substVariance x Unused = Unused
substVariance Mixed x = Mixed

data VarianceError =
   BadFunctorVariance Const Variance
 | RecDatatypeArgsNotParams Const
 | VarianceErrorsInImport Extent 

{- compute a list of variance errors.  Also return helpful other data structures; see VS below -}
variances :: ScopeMap -> FileWithDeps -> ([VarianceError],VS)
variances sm f = runState (variancesFileWithDeps sm f) (M.empty,M.empty)

-- try to return the variance of a type constant introduced at the given position
variance :: VarianceMap -> Pos -> Maybe Variance
variance vm p = M.lookup p vm

-- try to return a list of variances for a type constant introduced at the given position
paramVariances :: VariancesMap -> Pos -> Maybe [Variance]
paramVariances vsm p = M.lookup p vsm

showParamVariances :: [Variance] -> String
showParamVariances vs = "(message \"variances: " ++ intercalate " " (map show vs) ++ "\")"

showVariance :: Variance -> String
showVariance v = "(message \"variance: " ++ show v ++ "\")"

startVarianceError :: Extent -> String
startVarianceError (sp,ep) =
  "(dcs-mode-variance-error " ++ elispPos sp ++ " " ++ elispPos ep ++ " "

endVarianceError :: String
endVarianceError = "\\n\")"

showVarianceError :: VarianceError -> String
showVarianceError (BadFunctorVariance c v) =
  startVarianceError (extentConst c) ++ "\"  Recursive occurrence of " ++
  constStr c ++ " in its definition is at disallowed variance " ++ show v ++ endVarianceError
  
showVarianceError (RecDatatypeArgsNotParams c) =
  startVarianceError (extentConst c) ++ "\"  Arguments to recursive occurrence of " ++
  constStr c ++ " must be just the type parameters" ++ endVarianceError

showVarianceError (VarianceErrorsInImport ext) =
  startVarianceError ext ++ "\"  " ++ "Variance errors in the imported file or its imports" ++
  endVarianceError 

{- map the positions of declared type constants to
   a list of polarities, one for each of their 
   type parameters.

   We need this so we can check variances for TApps

   For functors of datatypes, we do not explicitly
   list the functorial input as a parameter.
-}
type VariancesMap = M.Map Pos [Variance] 

-- map positions of type parameters to their Variance.
type VarianceMap = M.Map Pos Variance

-- Variances State
type VS = (VariancesMap,VarianceMap)

{- Compute the occurrences of datatypes that are negative in their own definitions.

   Along the way, compute a VariancesMap for the
   declared type constants.

   The ScopeMap is for mapping occurrences of type parameters
   to their binding occurrences (this is used for variancesTy
   below).
-}

type Variances a = ScopeMap -> a -> State VS [VarianceError]

variancesFileWithDeps :: Variances FileWithDeps
variancesFileWithDeps sm = processFileWithDeps (variancesSimpleFile sm) VarianceErrorsInImport 

variancesSimpleFile :: Variances SimpleFile
variancesSimpleFile sm f = join <$> mapM (variancesStatement sm) f

variancesStatement :: Variances Statement
variancesStatement sm (TyDefSt d) = variancesTyDef sm d
variancesStatement sm (DataDefSt d) = variancesDataDef sm d
variancesStatement _ (TmDefSt d) = return [] -- nothing we need to check, for term-level definitions

{- given a VarianceMap, compute the list of Variances for some parameters.
   Parameters that are unmapped by the VarianceMap are mapped to Unused. -}
variancesForParams :: VarianceMap -> [Var] -> [Variance]
variancesForParams vm = map (\ param ->
                               case M.lookup (varPos param) vm of
                                 Nothing -> Unused
                                 Just v -> v) 

variancesTyDef :: Variances TyDef
variancesTyDef sm (TyDef _ c params ty) = 
  variancesBridge sm [] c params [ty]

variancesDataDef :: Variances DataDef
variancesDataDef sm (DataDef _ c params cs) =
  -- we will add a parameter cp for the functorial argument of the datatype's signature functor
  -- we need to include its variance in the list of variances we store for c
  let cp = ("*",constPos c) in
    variancesBridge sm [(c,params,cp)] {- add more to this list if we add mutually recursive types -}
    c (params ++ [cp]) (typesFromCtrs cs)

{- helper function abstracting most of variancesTyDef and variancesDataDef,
   bridging from Variances functions above to variancesTy

   The constants 'dts' are the datatypes (could be more than one if mutual)
   that we can only see in functorial positions (because we are checking
   a datatype definition), with their parameters.
-}
type DatatypeRecInfo = (Const,[Var],Const)

variancesBridge :: ScopeMap -> [DatatypeRecInfo] ->
                   Const -> [Var] -> [Ty] -> State VS [VarianceError]
variancesBridge sm dts c params tys =
  do
    (vsm,vm) <- get

    let (ps,vm') = runState (concatMapM (variancesTy sm dts vsm Positive) tys) vm
        paramsv = variancesForParams vm' params 

    -- update the VariancesMap to map c to paramsv,
    -- and also add the variances for the params to the VarianceMap 
    put $ (M.insert (constPos c) paramsv vsm,
           foldr (\ (p,v) t -> M.insert (constPos p) v t) vm' (zip params paramsv))

    return ps

type VariancesInner a b = ScopeMap -> [DatatypeRecInfo] -> VariancesMap -> Variance -> a ->
                          State VarianceMap b

{- given a Variance compute list of positions where * is used
   negatively in the given Ty.  Use the given VarianceMap to
   handle TApps that occur in the Ty.
   
   Along the way, compute a VarianceMap, showing how the type
   parameters encountered are used.

   We need a ScopeMap here, because our VarianceMap needs to
   map just binding occurrences of type parameters, so we need
   to be able to lookup such occurrences from a use of a type
   variable.
-}
variancesTy :: VariancesInner Ty [VarianceError]
variancesTy _ _ _ _ (TAppMeta _) = return []
variancesTy sm dts vsm v (Arrow _ _ t1 t2) =
  aseq [ variancesTy sm dts vsm (invertVariance v) t1, 
         variancesTy sm dts vsm v t2 ]
variancesTy sm dts vsm v (TApp (c,tys)) =
  do
    (ps,vs) <- variancesConst tys sm dts vsm v c

    {- for each of argument position of c of variance v',
       check variances for the substVariance-combination of v and v'
       in the type argument ty.

      Note that we are not trying to catch kinding errors here, so
      if c has more or fewer type parameters than we have arguments here,
      we just ignore the excess parameters or arguments. -}          
    aseq [return ps,
          concatMapM (\ (v',ty) -> variancesTy sm dts vsm (substVariance v v') ty)
            (zip vs tys)
          ]
variancesTy sm dts vsm v (TyParens _ t) = variancesTy sm dts vsm v t

{- if c is a type parameter, update its variance.

   Otherwise, if c is an occurrence of a datatype whose definition we are currently checking (so
   its binding occurence is in dts) and it is used at the wrong variance, return c (as a
   variance error).  The first argument is a list of types that we should confirm are variables
   whose binding occurrences are the parameters to the datatype.  This is to prevent GADTs.
   Also, record the variance v for the constant cp stored in dts for c.

  If c is a previously declared type constant, return its variances.
-}
variancesConst :: [Ty] -> VariancesInner Const ([VarianceError],[Variance])
variancesConst tyargs sm dts vsm v c =
  case M.lookup (constPos c) sm of
    Nothing -> return ([],[]) -- undefined variable, not a variance problem
    Just c' ->
      case find (\ (x,_,_) -> x == c') dts of
        Just (_,params,cp) -> 
          -- c' is a datatype whose definition we are currently checking

          -- get the binding occurrences of all tyargs that are consts
          let constargs = bindingIf sm <$> constsFromTys tyargs
              errs = 
                (if not (okFunctorVariance v)
                 then [ BadFunctorVariance c v ]
                 else [])
                ++
                (if params /= constargs
                 then [RecDatatypeArgsNotParams c]
                 else [])
          in
            
            updateVariance (constPos cp) v >>
            return (errs,[]) {- no need to return any variances back to check,
                                as we just confirmed we are applying c
                                only to the type params of c' -}
        Nothing ->
          case M.lookup (constPos c') vsm of
            Nothing ->
              -- c is not in our VariancesMap, so it must be a type parameter
              updateVariance (constPos c') v >> return ([],[])
            Just vs ->
              -- c is a previously declared type constant.
              return ([], vs)

-- update the type parameter introduced at position p with the meet of v and its current variance
updateVariance :: Pos -> Variance -> State VarianceMap ()
updateVariance p v =
  do
    vm <- get
    let vp = case M.lookup p vm of
               Nothing -> v
               Just v' -> glbVariance v v' 
    put $ M.insert p vp vm
